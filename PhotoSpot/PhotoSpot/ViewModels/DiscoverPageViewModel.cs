﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Prism;
using Prism.Mvvm;

//using Xamarin.Forms;

namespace PhotoSpot.ViewModels
{
    public class DiscoverPageViewModel : BindableBase, IActiveAware
    {
        public event EventHandler IsActiveChanged;

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                SetProperty(ref _isActive, value);
                IsActiveChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public DiscoverPageViewModel()
        {
            Title = "Discover";
            IsActiveChanged += OnIsActiveChanged;
        }

        void OnIsActiveChanged(object sender, EventArgs emptyEventArgs)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnIsActiveChanged)}:  {IsActive}");
        }
    }
}

