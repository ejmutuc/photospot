﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;
using Prism;
using Prism.Mvvm;
using PhotoSpot.Helpers;
using PhotoSpot.Models;
using System.Collections.ObjectModel;
using System.Linq;

//using Xamarin.Forms;

namespace PhotoSpot.ViewModels
{
    public class CollectionPageViewModel : BindableBase, IActiveAware
    {
        public event EventHandler IsActiveChanged;

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                SetProperty(ref _isActive, value);
                IsActiveChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public CollectionPageViewModel()
        {
            Title = "Collection";
            PhotoLocations = LocationHelper.PhotoLocations;
            PhotoLocationsGrouped = LocationHelper.PhotoLocationsGrouped;
            IsActiveChanged += OnIsActiveChanged;
        }

        void OnIsActiveChanged(object sender, EventArgs emptyEventArgs)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnIsActiveChanged)}:  {IsActive}");
        }

        public ObservableCollection<PhotoLocations> PhotoLocations { get; set; }
        public ObservableCollection<Grouping<string, PhotoLocations>> PhotoLocationsGrouped { get; set; }

        public int PhotoLocationsCount => PhotoLocations.Count;



    }
}