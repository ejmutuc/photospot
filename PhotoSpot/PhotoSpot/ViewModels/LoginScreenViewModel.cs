﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using PhotoSpot.Views;

namespace PhotoSpot.ViewModels
{
    public class LoginScreenViewModel : BindableBase, INavigationAware, IDestructible
    {
        protected INavigationService _navigationService { get; private set; }
        public DelegateCommand NavigateToTabContainerCommand { get; set; }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public LoginScreenViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            NavigateToTabContainerCommand = new DelegateCommand(OnNavigateToTabContainer);
            Title = "Login Page";
        }

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {

        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {

        }

        private async void OnNavigateToTabContainer()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigateToTabContainer)}");

            await _navigationService.NavigateAsync(new Uri("/TabContainer", UriKind.Absolute));

        }

        public virtual void Destroy()
        {

        }
    }
}
