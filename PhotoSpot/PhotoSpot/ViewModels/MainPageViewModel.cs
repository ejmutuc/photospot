﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using PhotoSpot.Views;
using PhotoSpot.Helpers;

namespace PhotoSpot.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware
    {
        INavigationService _navigationService;
		IPageDialogService _pageDialogService;

        public DelegateCommand NavigateToLoginScreenCommand { get; set; }
		public DelegateCommand ShowActionSheetCommand { get; set; }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

		public MainPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(MainPageViewModel)}:  ctor");

            _navigationService = navigationService;
			_pageDialogService = pageDialogService;
            NavigateToLoginScreenCommand  = new DelegateCommand(OnNavigateToLoginScreenCommand);
			ShowActionSheetCommand = new DelegateCommand(OnShowActionSheet);

			Title = "Main Page";
        }

		private async void OnShowActionSheet()
        {
			string userResponse = await _pageDialogService.DisplayActionSheetAsync(Constants.ACTION_TITLE,
			                                                                       Constants.ACTION_CANCEL,
			                                                                       null,
			                                                                       Constants.ACTION_NEW_USER,
			                                                                       Constants.ACTION_EXISTING_USER);

			if (userResponse.Equals(Constants.ACTION_NEW_USER, StringComparison.OrdinalIgnoreCase))
            {
				await _navigationService.NavigateAsync(new Uri("CreateAccountPage", UriKind.Relative));
            }

			else if (userResponse.Equals(Constants.ACTION_EXISTING_USER, StringComparison.OrdinalIgnoreCase))
            {
				await _navigationService.NavigateAsync(new Uri("LoginPage", UriKind.Relative));;
            }
        

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnShowActionSheet)}:  User response:  {userResponse}");
        }


        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }

        private async void OnNavigateToLoginScreenCommand()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigateToLoginScreenCommand)}");

            await _navigationService.NavigateAsync(nameof(LoginPage));

        }
    }
}
