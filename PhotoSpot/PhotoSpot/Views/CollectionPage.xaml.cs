﻿using System;
using System.Collections.Generic;
using PhotoSpot.ViewModels;
using Xamarin.Forms;
using PhotoSpot.Models;

namespace PhotoSpot.Views
{
    public partial class CollectionPage : ContentPage
    {
        public CollectionPage()
        {
            InitializeComponent();
            BindingContext = new SearchPageViewModel();
        }

        void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        => ((ListView)sender).SelectedItem = null;

        void Handle_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var photoLoc = ((ListView)sender).SelectedItem as PhotoLocations;
            if (photoLoc == null)
                return;


        }

        void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}