﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PhotoSpot.Models;
using System.Linq;

namespace PhotoSpot.Helpers
{
    public static class LocationHelper
    {

        private static Random random;

        public static PhotoLocations GetRandomLocation()
        {
            //var output = Newtonsoft.Json.JsonConvert.SerializeObject(Monkeys);
            return PhotoLocations[random.Next(0, PhotoLocations.Count)];
        }


        public static ObservableCollection<Grouping<string, PhotoLocations>> PhotoLocationsGrouped { get; set; }

        public static ObservableCollection<PhotoLocations> PhotoLocations { get; set; }

        static LocationHelper()
        {
            random = new Random();
            PhotoLocations = new ObservableCollection<PhotoLocations>();
            PhotoLocations.Add(new PhotoLocations
            {
                Name = "Oceanside Pier",
                Location = "Oceanside, CA",
                Details = "At 1,942 feet, the historical Oceanside Pier is one of the longest wooden piers on the West Coast.  Take a leisurely stroll out over the Pacific Ocean, watch the surfers ride a wave, fishermen catch a fish, or grab a bite to eat at the famous Ruby’s Diner located at the end of the pier.",
                Image = "http://visitoceanside.org/wp-content/uploads/2017/02/Pier-high-res-small.jpg"
            });

            PhotoLocations.Add(new PhotoLocations
            {
                Name = "Carlsbad Flower Fields",
                Location = "Carlsbad, CA",
                Details = "The Flower Fields is a flower garden found on the Carlsbad Ranch in Carlsbad, California. It opens up once a year in spring from March 1 through May 10.",
                Image = "http://www.sandiegomagazine.com/images/cache/cache_b/cache_7/cache_3/carlsbad-flower-fields-19c3037b.jpeg"
            });

            PhotoLocations.Add(new PhotoLocations
            {
                Name = "Salk Institute",
                Location = "La Jolla, CA",
                Details = "The Salk Institute for Biological Studies is an independent, non-profit, scientific research institute located in La Jolla, San Diego, California, United States",
                Image = "https://www.salk.edu/wp-content/uploads/2015/10/0X8C0017_8_9_tonemapped_p-_720x480_72_RGB.jpg"
            });


            PhotoLocations.Add(new PhotoLocations
            {
                Name = "Balboa Park",
                Location = "San Diego, CA",
                Details = "Balboa Park is a 1,200-acre urban cultural park in San Diego, California, United States. In addition to open space areas, natural vegetation zones, green belts, gardens, and walking paths, it contains...",
                Image = "https://cdn.trolleytours.com/wp-content/uploads/2016/06/san-diego-balboa-park-480x270.jpg"
            });

            PhotoLocations.Add(new PhotoLocations
            {
                Name = "Griffith Observatory",
                Location = "Los Angeles, CA",
                Details = "Griffith Observatory is a facility in Los Angeles, California, sitting on the south-facing slope of Mount Hollywood in Los Angeles' Griffith Park. .",
                Image = "http://1.bp.blogspot.com/-BZz-WgM9LnA/TVaxQrvy_fI/AAAAAAAAFd8/vKnxHslpZAo/s1600/Griffith%2BObservatory%2BWally%2BSkalij%2B%253A%2BLos%2BAngeles%2BTimes.jpg"
            });

            PhotoLocations.Add(new PhotoLocations
            {
                Name = "Urban Lights",
                Location = "Los Angeles, CA",
                Details = "Urban Light is a large-scale assemblage sculpture by Chris Burden located at the Wilshire Boulevard entrance to the Los Angeles County Museum of Art. The 2008 installation consists of restored street lamps from the 1920s and 1930s.",
                Image = "https://c1.staticflickr.com/6/5626/21547561124_41404b4204_b.jpg"
            });

            //PhotoLocations.Add(new PhotoLocations
            //{
            //    Name = "Japanese Macaque",
            //    Location = "Japan",
            //    Details = "The Japanese macaque, is a terrestrial Old World monkey species native to Japan. They are also sometimes known as the snow monkey because they live in areas where snow covers the ground for months each",
            //    Image = "http://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Macaca_fuscata_fuscata1.jpg/220px-Macaca_fuscata_fuscata1.jpg"
            //});

            //PhotoLocations.Add(new PhotoLocations
            //{
            //    Name = "Mandrill",
            //    Location = "Southern Cameroon, Gabon, Equatorial Guinea, and Congo",
            //    Details = "The mandrill is a primate of the Old World monkey family, closely related to the baboons and even more closely to the drill. It is found in southern Cameroon, Gabon, Equatorial Guinea, and Congo.",
            //    Image = "http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Mandrill_at_san_francisco_zoo.jpg/220px-Mandrill_at_san_francisco_zoo.jpg"
            //});

            //PhotoLocations.Add(new PhotoLocations
            //{
            //    Name = "Proboscis Monkey",
            //    Location = "Borneo",
            //    Details = "The proboscis monkey or long-nosed monkey, known as the bekantan in Malay, is a reddish-brown arboreal Old World monkey that is endemic to the south-east Asian island of Borneo.",
            //    Image = "http://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Proboscis_Monkey_in_Borneo.jpg/250px-Proboscis_Monkey_in_Borneo.jpg"
            //});


            var sorted = from photolocations in PhotoLocations
                         orderby photolocations.Name
                         group photolocations by photolocations.NameSort into photoLocationsGroup
                         select new Grouping<string, PhotoLocations>(photoLocationsGroup.Key, photoLocationsGroup);

            PhotoLocationsGrouped = new ObservableCollection<Grouping<string, PhotoLocations>>(sorted);

        }
    }
}

