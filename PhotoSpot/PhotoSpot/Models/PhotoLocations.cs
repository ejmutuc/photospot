﻿namespace PhotoSpot.Models
{
    public class PhotoLocations
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string Details { get; set; }
        public string Image { get; set; }

        public string NameSort => Name[0].ToString();
    }
}

